package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CPerson extends CAnimal{

    private int id;
    private int age;
    private String firstname;
    private String lastname;
    private ArrayList<CPet> pets;



    public CPerson() {
        super();
    }

   
    
    /** 
    *Khởi tạo Person với các tham số
   * @param id
   * @param age
   * @param firstname
   * @param lastname
   * @param pets
    */

    public CPerson(int id, int age, String firstname, String lastname, ArrayList<CPet> pets) {
        super();
        this.id = id;
        this.age =age;
        this.firstname = firstname;
        this.lastname = lastname;
        this.pets = pets;

    }



    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("person is speaking...");
    }
    /**
     * @return id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id the id tp set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return age
     */

    public int getAge() {
        return age;
    }



    public void setAge(int age) {
        this.age = age;
    }



    public String getFirstname() {
        return firstname;
    }



    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }



    public String getLastname() {
        return lastname;
    }



    public void setLastname(String lastname) {
        this.lastname = lastname;
    }



    public ArrayList<CPet> getPets() {
        return pets;
    }



    public void setPets(ArrayList<CPet> pets) {
        this.pets = pets;
    }
    
    @Override
    public String toString(){
        return "CPerson{id: " + this.id + ", age " + this.age + ", firstname " + this.firstname + ", lastname " + this.lastname + ", pets " + this.pets + " }";
    }

    public static void main(String[] args) {
        CAnimal newFish = new CFish();
        CPet newBird =new CBird();
        CPet newCat = new CCat();
        CPet newDog = new CDog();
        CPerson newPerson = new CPerson();
        newPerson.setAge(20);
        ArrayList<CPet> petList = new ArrayList<>();
        petList.add((CPet)newFish);
        petList.add(newBird);
        petList.add(newCat);
        petList.add(newDog);
        newPerson.setPets(petList);
        System.out.println(newPerson);

    }



    public CPerson(int id) {
        this.id = id;
    }



    public CPerson(int id, int age) {
        this.id = id;
        this.age = age;
    }

}
