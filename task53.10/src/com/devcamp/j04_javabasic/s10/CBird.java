package com.devcamp.j04_javabasic.s10;

public class CBird extends CPet implements IFlyable{
    @Override
    public void fly() {
        System.out.println("bird is flying...");
    }

    public static void main(String[] args) {
        CBird myBird = new CBird();
        myBird.age = 2;
        myBird.name = "My Eagle";
        myBird.animalClass = AnimalClass.birds;
        myBird.eat();
        myBird.animalSound();
        myBird.play();
        myBird.print();
        myBird.fly();
    }
}
