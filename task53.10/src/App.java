import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.CIntegerArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        CIntegerArrayList newList = new CIntegerArrayList();
        ArrayList<Integer> intList = new ArrayList<>();
        intList.add(10);
        intList.add(20);
        intList.add(30);
        intList.add(40);

        newList.setmIntegerArrayList(intList);
        newList.getSum();
        System.out.println(newList.getSum());
    }
}
